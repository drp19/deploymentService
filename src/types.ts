export interface TOKENS
{
    [key: string]: TOKEN;
}

export interface DATA
{
    port: number
}

interface TOKEN 
{
    docker: boolean,
    dir: string,
    token: string,
    script: string|null
}