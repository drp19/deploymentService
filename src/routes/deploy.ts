/* Initializing dependencies */
import express from 'express';
import { TOKENS } from '../types';
const { exec } = require("child_process");

const route = express.Router();

const deployHandler = (tokens: TOKENS): express.Router =>
{
    route.get("/", (req, res) => {
        let auth = req.headers.authorization;
        if (!auth) {
            return res.status(400).send("no auth header");
        }
        let bearer = auth.split(' ');
        if (bearer.length != 2) {
            return res.status(400).send("invalid auth format");
        }
        let bearerToken = bearer[1];
        let buff = new Buffer(bearerToken, 'base64');
        let decoded = buff.toString("ascii");
        
        let parts = decoded.match(/(\w+)\:(.*)/);
        if (!parts || parts.length != 3) {
            return res.status(400).send("invalid auth format");
        }
        let serv = parts[1];
        let token = parts[2];
        let servObject = tokens[serv];
        if (!servObject) {
            return res.status(400).send("unknown service");
        }
        
        let servToken = servObject.token;
        let servDir = servObject.dir;
        let servScript: string;
        if (servObject.docker || servObject.script === null) {
            servScript = "./scripts/docker.sh " + servDir;
        } else {
            servScript = servObject.script!;
        }

        if (token != servToken) {
            return res.status(401).send("unauthorized");
        }

        exec(servScript, (error: { message: any; }, stdout: any, stderr: any) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });

        return res.status(200).send("ok");
    });
	return route;
};

export default deployHandler;
