// Initializing Dependencies

import fs from 'fs';

// express web framework
import express from 'express';
const app = express();
app.disable('x-powered-by'); // hide server software information from response header - better for security
const morgan = require('morgan')
// routings
import Routes from './routes';
import { TOKENS, DATA } from './types';
// main function
async function main()
{
	// log the date started
	// eslint-disable-next-line no-console
	console.log();
	// eslint-disable-next-line no-console
	console.log(new Date().toDateString() + ' ' + new Date().toTimeString());
	
	const tokens = await JSON.parse(fs.readFileSync("./data/tokens.json", 'utf-8')) as TOKENS;
	const data = await JSON.parse(fs.readFileSync("./data/data.json", 'utf-8')) as DATA;
	
	// port
	const port = data.port;

	app.use(morgan('combined'))
	
    // Dynamic Routes
    const deploy = Routes.deploy(tokens);
	app.use("/deploy", deploy);
	
	// Starting Server
	app.listen(port, () =>
	{
		// eslint-disable-next-line no-console
		console.log(`The server is ready! Running on http://localhost:${port}`);
	});
}

void main(); // run the main function
