#!/bin/bash

if [[ $# != 1 ]]; then
    echo "Pass the working directory as a parameter."
    exit 1
fi
cd $1
# get latest revision
git pull
# build docker
docker-compose up --build -d
echo "Done"