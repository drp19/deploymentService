# Deployment Service
This is a very basic service to take input from a URL and then run a script based on the authorization header.
Scripts are all local and not from the request.

**VERY BASIC AND EARLY VERSION**

### How to Use

1) Following the example, create a token in `data/tokens.json`. Either a) you set `docker` to `true` and provide the directory in which the repo is located, or b) you set `docker` to `false` and provide a path to a script to run that performs custom actions.
2) Run with `npm start`.
3) Send a GET request to `/deploy` with a standard format authorization header. The username is the service to deploy (the high level name in the tokens file) and the password is the service token you specify.

#### For Docker Users:
I tried to make this service be able to run in a container and be able to upgrade itself, but ultimately it was too complex. However, from that endeavor I do have the `Dockerfile` and `docker-compose.yaml` involved in that and can be found [here](https://gitlab.com/-/snippets/2098418) (and also in the commit history).
In short, will pull the latest version for a given repo directory, and then run docker-compose on it, so set up that however works for you. I also specified the port in a `.env` file, which should help make your code independent of the system it is on.